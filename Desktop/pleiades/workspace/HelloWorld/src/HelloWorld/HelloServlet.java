package HelloWorld;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/HelloJsp")
public class HelloServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {

			System.out.println(getCurrentTime());

		request.setAttribute("message", "Hello, World!");
		// request.setAttribute("message", CurrentTime());

		RequestDispatcher rd = request.getRequestDispatcher("/HelloJsp.jsp");

		rd.forward(request, response);

		} finally {
			System.out.println(getCurrentTime());
		}
	}

	private String getCurrentTime() {
		return new SimpleDateFormat("HH:mm:ss").format(new Date());
	}



}
